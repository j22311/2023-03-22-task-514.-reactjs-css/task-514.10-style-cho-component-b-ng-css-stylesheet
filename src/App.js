import './App.css'

function App() {
  return (
    <div className="devcamp">
      <div className='decamp-wrapper'>
      <img className='devcamp-avatar '  src='https://randomuser.me/api/portraits/women/48.jpg' />

      <div className="devcamp-quote">
      <p>this is one of the best developers blogs on the planet! i read it daily to improve my skils.</p>
      </div>
      <p className="devcamp-name"><span>tammy Stevens.</span> front End developer</p>
      </div>
    </div>
  );
}

export default App;
